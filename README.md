
[本项目的Git仓库]: https://gitee.com/guobinyong/option_model
[option-model的接口文档]: ./option-model的接口文档.md



目录
=======================
- 一、简介
- 二、安装方式
   - 方式1：通过 npm 安装
   - 方式2：直接下载原代码
- 三、概念讲解
- 四、创建一个选择题
- 五、创建一个选择组
- 四、选项 Option
   - 构造函数
   - 创建选项
- 五、选择题 ChoiceQuestion
   - 构造函数
   - 创建选择题
   - 选择操作和设置
   - 查找和删除操作
   - 条件测试
   - 计分
- 六、选择题组 ChoiceQuestionList
   - 创建选择组
   - 题目汇总
   - 选择操作
   - 查找和删除操作
   - 条件测试

内容
================





# 一、简介
在很多需要中，选择题是很常见的功能，如：调查问卷、选项设置、选项验证 等等；无论对于什么样的业务，虽然选择题的页面效果、交互、规则 等都不一样，但是，经过合理的设置，完全可以抽离出一套选择题的逻辑、数据模型； option-model 就是这样一个模型；

option-model 作为 选择题的数据、逻辑模型；支持单选、多选、限制选、默认选择、管理、统计、汇总、测试、验证、打分 等等；

**相关文档：**  
- 《[option-model的接口文档][]》


**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe


# 二、安装方式
目前，安装方式有以下几种：


## 方式1：通过 npm 安装
```
npm install --save option-model
```

## 方式2：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，此仓库里包含了 option-model 和 下文的示例代码； option-model 库是 [本项目的Git仓库][] 项目中的 package/ChoiceQuestionModel.js 文件，您可以直接把该文件拷贝到您的项目中去；然后使用如下代码在您的项目中引入你需要的类：
```
import {ChoiceQuestion,Option,ChoiceQuestionList,ChoiceCondition} from "path/to/package/ChoiceQuestionModel.js";
```



# 三、概念讲解
对于选择题业务，肯定会有这些概念：题、题的选项、一组题； 在 option-model 中，也这对应的概念，并把这些概念设计成了类，如下：

- ChoiceQuestion : 选择题类，表示一个选择题；
- Option : 选项类，表示选择题的选项；
- ChoiceQuestionList : 选择题组类，表示一组选择题，即选择题列表；
- ChoiceCondition : 选择题约束类，表示选择题的限制条件； 该类是个抽象类， 用于让其它类继承； 选择题类 ChoiceQuestion 也继承自 选择题约束类 ChoiceCondition ；


在 option-model 的概念中，多了个 选择题约束类 ChoiceCondition ，一般用不到这个类；

**以上各类的详细信息请见《[option-model的接口文档][]》**




# 四、选项 Option
在选择题中，选项就是选择题提供的供用户选择的答案；在 option-model 中，用 Option 类 表示选项；

## 构造函数
`Option(content,id,choiceQuestion)`
- @param content : any     选项的内容
- @param id : any     选项的标识符，在一个题目中，需要唯一
- @param choiceQuestion : ChoiceQuestion   选择所属的题目

## 创建选项
选项的创建和设置有2种方式

- 方式1：用构造函数创建和配置
   ```
   let option = new Option("河南人",1);
   ```

- 方式2：先创建未配置的选项，再更改相关属性：
   ```
   let option = new Option();  //先创建选项
   option.content = "安徽人";    //设置选项的内容
   option.id = "A";    //设置选项的标识符
   ```



# 五、选择题 ChoiceQuestion
选择题 是 问题 及 该问题提供的供用户选择的答案 一个集合；即：选择题 包含 问题 和 选项列表； 在 option-model 中，用 ChoiceQuestion 类 表示选择题；


## 构造函数
`ChoiceQuestion(subject,minSelectCount,maxSelectCount,id,options,rightAnswerIds,defaultSelectedIds,scoreValuel,type)`
- @param subject : any     选择题的问题、标题；
- @param minSelectCount : number 最小选择数目
- @param maxSelectCount : number 最大选择数目
- @param id : any   题目的唯一标识符
- @param options : Array<Option> = []  盛装选择题的所有选项的数组
- @param rightAnswerIds : Array = []  盛装正确答案id的数组
- @param defaultSelectedIds : Array = []  盛装默认选中的答案id的数组
- @param scoreValuel : number = 1    题目的分值
- @param type : any    题目类型



## 创建选择题
选择题的创建和设置有2种方式

- 方式1：用构造函数创建和配置
   ```
   let question = new ChoiceQuestion("你是哪里人？",1,2,1,optionArr);
   ```

- 方式2：先创建未配置的选项，再更改相关属性：
   ```
   let question = new ChoiceQuestion();   //先创建题目
   question.subject = "你爱人是哪里人？";     //设置标题
   question.minSelectCount = 1;   //设置最小选择数目
   question.maxSelectCount = 3;   //设置最大选择数目
   question.id = 1;   //设置id
   question.options = optionArr;  //设置选项
   ```


## 选择操作和设置
你可以通过 `defaultSelectedIds` 属性来设置 选择题默认的选中的选项；通过 `selectedOptions` 或者 `selectedOptionIds` 获取选择题中被选中的选项； 

ChoiceQuestion 的实例有丰富的选中方法，如下：
- selectOptionOfId(optionID) : 选中 id 为 optionID 的选项
- selectOptionsOfIdList(idList) : 选中 idList 中指定的所有 选项
- onlySelectOptionsOfIdList(idList) : 只选中 idList 中指定的所有 选项
- onlySelectOptionsOutofIdList(idList) : 只选中除 idList 中指定的 之外的 所有选项
- deselectOptionOfId(optionID) : 取消选中 id 为 optionID 的选项
- deselectOptionsOfIdList(idList) : 取消选中 idList 中指定的所有 选项
- deselectAll() : 取消所有选项的选中
- resetSelect() : 重围选中，重置后只选中默认选中的选项


## 查找和删除操作
ChoiceQuestion 的实例中也提供了丰富的查找、删除方法，如下：

- findIndexForId(optionID) : 返回 id 为 optionID 的 选项 在 options 中的索引；如果没有符合条件的元素返回 -1
- findOptionForId(optionID) : 返回 id 为 optionID 的 选项；如果没有符合条件的元素返回 undefined
- findIndexListForIdList(idList)- : 返回 idList 中的 id 对应的 选项 在 options 中的索引的数组；
- findOptionListForIdList(idList) : 返回 idList 中的 id 对应的 选项 的数组；
- deleteOptionForId(optionID) : 删除 id 为 optionID 的 选项 ；
- deleteOptionForIdList(idList) : 删除 idList 中指定的所有选项 ；




## 条件测试
ChoiceQuestion 的实例有丰富的条件测试方法，有以下方法可用于条件测试：
- testRequisite() : 测试是否满足必须
- testMinSelectCount() : 测试是否满足最小选择数
- testMaxSelectCount() : 测试是否满足最大选择数
- testAllCondition() : 测试是否满足所有的约束条件
- testAnswer() : 测试是否回答正确，需要设置 rightAnswerIds



## 计分
如果给选择题 ChoiceQuestion 实例设置了 分值 scoreValuel ，则也可以通过ChoiceQuestion 实例的 score 属性 来获得得分情况；







# 六、选择题组 ChoiceQuestionList
在选择题的使用场景中，往往会有很多道选择题，多个选择题的集合 就称为 **选择题组** 或者 **选择题列表** ； 在 option-model 中，用 ChoiceQuestionList 类 表示选择题组；



## 创建选择组
因为 ChoiceQuestionList 继承自 Array ，所以 ChoiceQuestionList 的构造函数 与 Array 相同；所以创建选择题组的方式也有很多种，分别如下：


- 方式1:
   ```
   let questionList = new ChoiceQuestionList();    //先创建一个空的选择题组
   //再把选择题实例一个一个的 push 进 选择题组
   questionList.push(question1); 
   questionList.push(question2);
   questionList.push(question3);
   ```

- 方式2:
   ```
   let questionList = new ChoiceQuestionList(question1,question2,question3);  //创建选择题组实例时 直接把选择题作为参数传入构造函数
   ```

- 方式3:
   ```
   let questionList = new ChoiceQuestionList(3);  //先创建相应长度的空的选择题组
   // 再通过的方括号语法设置相应的选择题
   questionList[0] = question1;
   questionList[1] = question1;
   questionList[2] = question1;
   ```

当然，还有其它方式，这里不再一一介绍了！



## 题目汇总
通过 ChoiceQuestionList 实例，可以查看各种关于题目的统计、汇总数据，如下：
- passConditionOfQuestions : 获得符合约束条件的所有题目的列表
- passConditionOfQuestionIds : 获得符合约束条件的所有题目的id列表
- notPassConditionOfQuestions : 获得不符合约束条件的所有题目的列表
- notPassConditionOfQuestionIds : 获得不符合约束条件的所有题目的id列表
- answerRightQuestions : 获得回答正确的题目的列表
- answerRightQuestionIds : 获得回答正确的题目的id列表
- answerWrongQuestions : 获得回答错误的题目的列表
- answerWrongQuestionIds : 获得回答错误的题目的id列表
- totalScore : 所有题目的得分总和



## 选择操作
ChoiceQuestionList 的实例也有常用的选择操作的方法，如下：
- deselectAll() : 取消所有题目的选中
- resetSelect() : 重围所有题目的选中，重置后每个题目只选中默认选中的选项


## 查找和删除操作
虽然 ChoiceQuestionList 继承自 Array ，已经具备了 Array 所有的方法，但为了更方便您的使用，ChoiceQuestionList 的实例中仍然提供了丰富的查找、删除方法，如下：
- findIndexForId(questionID) : 返回 id 为 questionID 的 选择题 在 选择题组 中的索引；如果没有符合条件的元素返回 -1
- findQuestionForId(questionID) : 返回 id 为 questionID 的 选择题；如果没有符合条件的元素返回 undefined
- findIndexListForIdList(idList) : 返回 idList 中的 id 对应的 选择题 在 选择题组 中的索引的数组；
- findQuestionListForIdList(idList) : 返回 idList 中的 id 对应的 选择题 的数组；
- deleteQuestionForId(questionID) : 删除 id 为 questionID 的 选择题；
- deleteQuestionForIdList(idList) : 删除 idList 中指定的所有选择题 ；


## 条件测试
ChoiceQuestionList 的实例有丰富的条件测试方法，有以下方法可用于条件测试：
- testEveryAccordCondition() : 测试是否每一个题目都符合约束
- testSomeAccordCondition() : 测试是否有部分题目符合约束
- testEveryIsRight() : 测试是否每个题目都回答正确
- testSomeIsRight() : 测试是否部分题目回答正确

