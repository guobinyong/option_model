import {ChoiceQuestion,Option,ChoiceQuestionList,ChoiceCondition} from '../choice-question-model/ChoiceQuestionModel.js';



let option = new Option();  //先创建选项
option.content = "安徽人";    //设置选项的内容
option.id = "A";    //设置选项的标识符


let option1 = new Option("河南人",1);
let option2 = new Option("山东人",2);
let option3 = new Option("胡北人",3);
let option4 = new Option("黑龙江人",4);

let optionArr = [option1,option2,option3,option4];



let question = new ChoiceQuestion("你喜欢哪里人？",1,1,null,optionArr);

/*
let question2 = new ChoiceQuestion();   //先创建题目
question2.subject = "你爱人是哪里人？";     //设置标题
question2.minSelectCount = 1;   //设置最小选择数目
question2.maxSelectCount = 3;   //设置最大选择数目
question2.options = optionArr;  //设置选项


let questionList = new ChoiceQuestionList();    //先创建一个空的选择题组
//再把选择题实例一个一个的 push 进 选择题组
questionList.push(question1); 
questionList.push(question2);
questionList.push(question3);



let questionList = new ChoiceQuestionList(question1,question2,question3);  //创建选择题组实例时 直接把选择题作为参数传入构造函数


let questionList = new ChoiceQuestionList(3);  //先创建相应长度的空的选择题组
// 再通过的方括号语法设置相应的选择题
questionList[0] = question1;
questionList[1] = question1;
questionList[2] = question1;
 */

registerServiceWorker();