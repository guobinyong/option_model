option-model的接口文档
===================



Option
===============
- 说明 : 选项类，表示选择题提供的供用户选择的答案；


# 构造函数
`constructor(content,id,choiceQuestion)`
- @param content : any     选项的内容
- @param id : any     选项的标识符，在一个题目中，需要唯一
- @param choiceQuestion : ChoiceQuestion   选择所属的题目





# 属性
## id
- 特性 : 存储属性、可读写
- 类型 : any
- 说明 : 选项的标识符，在一个题目中，需要唯一


## content
- 特性 : 存储属性、可读写
- 类型 : any
- 说明 : 选项的内容


## choiceQuestion
- 特性 : 存储属性、可读写
- 类型 : ChoiceQuestion
- 说明 : 选择所属的题目


## selected
- 特性 : 存储属性、可读写
- 类型 : boolean
- 说明 : 是否选中



## defaultSelected
- 特性 : 计算属性、可读写
- 类型 : boolean
- 说明 : 是否默认选中









ChoiceCondition
===============
- 说明 : 选择题约束类，表示选择题的限制条件； 该类是个抽象类， 用于让其它类继承； 选择题类 ChoiceQuestion 也继承自 选择题约束类 ChoiceCondition ；

# 构造函数
`constructor(minSelectCount,maxSelectCount)`
- @param minSelectCount : number     最小选择数目
- @param maxSelectCount : number     最大选择数目



# 属性
## minSelectCount
- 特性 : 计算属性、可读写
- 类型 : number
- 说明 : 最小选择数目



## maxSelectCount
- 特性 : 计算属性、可读写
- 类型 : number
- 说明 : 最大选择数目



## requisite
- 特性 : 计算属性、只读
- 类型 : boolean
- 说明 : 是否必须选择


## multiselect
- 特性 : 计算属性、只读
- 类型 : boolean
- 说明 : 是否多选







ChoiceQuestion
===============
- 继承自：ChoiceCondition
- 说明 : 选择题类，表示一个选择题；


# 构造函数
`constructor(subject,minSelectCount,maxSelectCount,id,options,rightAnswerIds,defaultSelectedIds,scoreValuel,type)`
- @param subject : any     选择题的问题、标题；
- @param minSelectCount : number 最小选择数目
- @param maxSelectCount : number 最大选择数目
- @param id : any   题目的唯一标识符
- @param options : Array<Option> = []  盛装选择题的所有选项的数组
- @param rightAnswerIds : Array = []  盛装正确答案id的数组
- @param defaultSelectedIds : Array = []  盛装默认选中的答案id的数组
- @param scoreValuel : number = 1    题目的分值
- @param type : any    题目类型




# 属性
## type
- 特性 : 存储属性、可读写
- 类型 : any
- 说明 : 题目类型


## id
- 特性 : 存储属性、可读写
- 类型 : any
- 说明 : 题目的唯一标识符


## subject
- 特性 : 存储属性、可读写
- 类型 : any
- 说明 : 选择题的问题、标题；


## scoreValuel
- 特性 : 存储属性、可读写
- 类型 : number
- 说明 : 题目的分值；



## score
- 特性 : 计算属性、只读
- 类型 : number
- 说明 : 回答本题得的分数




## options
- 特性 : 计算属性、可读写
- 类型 : [Option]
- 说明 : 盛装选择题的所有选项的数组


## rightAnswerIds
- 特性 : 存储属性、可读写
- 类型 : Array
- 说明 : 盛装正确答案id的数组



## defaultSelectedIds
- 特性 : 计算属性、读写
- 类型 : Array
- 说明 : 默认选中的选项的id数组





## selectedOptions
- 特性 : 计算属性、只读
- 类型 : [Option]
- 说明 : 获得所有被选择的选项


## selectedOptionIds
- 特性 : 计算属性、只读
- 类型 : Array
- 说明 : 获得所有被选中的选项的id


# 方法
## testRequisite()
- 说明 : 测试是否满足必须


## testMinSelectCount()
- 说明 : 测试是否满足最小选择数


## testMaxSelectCount()
- 说明 : 测试是否满足最大选择数


## testAllCondition()
- 说明 : 测试是否满足所有的约束条件


## testAnswer()
- 说明 : 测试是否回答正确，需要设置 rightAnswerIds


 

## selectOptionOfId(optionID)
- @param optionID : any  需要被选中的选项的id
- 说明 : 选中 id 为 optionID 的选项


## selectOptionsOfIdList(idList)
- @param idList : Array  需要被选中的选项的id数组
- 说明 : 选中 idList 中指定的所有 选项


## onlySelectOptionsOfIdList(idList)
- @param idList : Array  需要被选中的选项的id数组
- 说明 : 只选中 idList 中指定的所有 选项


## onlySelectOptionsOutofIdList(idList)
- @param idList : Array  需要被排除选中的选项的id数组
- 说明 : 只选中除 idList 中指定的 之外的 所有选项



## deselectOptionOfId(optionID)
- @param optionID : any  需要被取消选中的选项的id
- 说明 : 取消选中 id 为 optionID 的选项



## deselectOptionsOfIdList(idList)
- @param idList : Array  需要被取消选中的选项的id数组
- 说明 : 取消选中 idList 中指定的所有 选项


## deselectAll()
- 说明 : 取消所有选项的选中


## resetSelect()
- 说明 : 重围选中，重置后只选中默认选中的选项



## findIndexForId(optionID)
- @param optionID : any 被查找的选项的id
- 说明 : 返回 id 为 optionID 的 选项 在 options 中的索引；如果没有符合条件的元素返回 -1



## findOptionForId(optionID)
- @param optionID : any 被查找的选项的id
- 说明 : 返回 id 为 optionID 的 选项 在 options 中的索引；如果没有符合条件的元素返回 undefined



## findIndexListForIdList(idList)
- @param idList : Array 被查找的选项的id列表
- 说明 : 返回 idList 中的 id 对应的 选项 在 options 中的索引的数组；




## findOptionListForIdList(idList)
- @param idList : Array 被查找的选项的id列表
- 说明 : 返回 idList 中的 id 对应的 选项 的数组；




## deleteOptionForId(optionID)
- @param optionID : any 被删除的选项的id
- 说明 : 删除 id 为 optionID 的 选项 ；




## deleteOptionForIdList(idList)
- @param idList : Array 被删除的选项的id列表
- 说明 : 删除 idList 中指定的所有选项 ；










ChoiceQuestionList
===============
- 继承自 : Array
- 说明 : 选择题组类，表示一组选择题，即选择题列表；
- 构造函数 : 与 Array 相同



# 属性

## passConditionOfQuestions
- 特性 : 计算属性、只读
- 类型 : [ChoiceQuestion]
- 说明 : 获得符合约束条件的所有题目的列表


## passConditionOfQuestionIds
- 特性 : 计算属性、只读
- 类型 : Array
- 说明 : 获得符合约束条件的所有题目的id列表


## notPassConditionOfQuestions
- 特性 : 计算属性、只读
- 类型 : [ChoiceQuestion]
- 说明 : 获得不符合约束条件的所有题目的列表



## notPassConditionOfQuestionIds
- 特性 : 计算属性、只读
- 类型 : Array
- 说明 : 获得不符合约束条件的所有题目的id列表




## answerRightQuestions
- 特性 : 计算属性、只读
- 类型 : [ChoiceQuestion]
- 说明 : 获得回答正确的题目的列表



## answerRightQuestionIds
- 特性 : 计算属性、只读
- 类型 : Array
- 说明 : 获得回答正确的题目的id列表



## answerWrongQuestions
- 特性 : 计算属性、只读
- 类型 : [ChoiceQuestion]
- 说明 : 获得回答错误的题目的列表


## answerWrongQuestionIds
- 特性 : 计算属性、只读
- 类型 : Array
- 说明 : 获得回答错误的题目的id列表



## totalScore
- 特性 : 计算属性、只读
- 类型 : number
- 说明 : 所有题目的得分总和












# 方法
## testEveryAccordCondition()
- 说明 : 测试是否每一个题目都符合约束


## testSomeAccordCondition()
- 说明 : 测试是否有部分题目符合约束


## testEveryIsRight()
- 说明 : 测试是否每个题目都回答正确


## testSomeIsRight()
- 说明 : 测试是否部分题目回答正确



## deselectAll()
- 说明 : 取消所有题目的选中


## resetSelect()
- 说明 : 重围所有题目的选中，重置后每个题目只选中默认选中的选项






## findIndexForId(questionID)
- @param questionID : any 被查找的选择题的id
- 说明 : 返回 id 为 questionID 的 选择题 在 选择题组 中的索引；如果没有符合条件的元素返回 -1



## findQuestionForId(questionID)
- @param questionID : any 被查找的选择题的id
- 说明 : 返回 id 为 questionID 的 选择题；如果没有符合条件的元素返回 undefined




## findIndexListForIdList(idList)
- @param idList : Array 被查找的选择题的id列表
- 说明 : 返回 idList 中的 id 对应的 选择题 在 选择题组 中的索引的数组；




## findQuestionListForIdList(idList)
- @param idList : Array 被查找的选择题的id列表
- 说明 : 返回 idList 中的 id 对应的 选择题 的数组；



## deleteQuestionForId(questionID)
- @param questionID : any 被删除的选择题的id
- 说明 : 删除 id 为 questionID 的 选择题；




## deleteQuestionForIdList(idList)
- @param idList : Array 被删除的选择题的id列表
- 说明 : 删除 idList 中指定的所有选择题 ；