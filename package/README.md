
[本项目的Git仓库]: https://gitee.com/guobinyong/option_model
[option-model的接口文档]: https://gitee.com/guobinyong/option_model/blob/master/option-model的接口文档.md

[option-model的教程]: https://gitee.com/guobinyong/option_model





# 一、简介
在很多需要中，选择题是很常见的功能，如：调查问卷、选项设置、选项验证 等等；无论对于什么样的业务，虽然选择题的页面效果、交互、规则 等都不一样，但是，经过合理的设置，完全可以抽离出一套选择题的逻辑、数据模型； option-model 就是这样一个模型；

option-model 作为 选择题的数据、逻辑模型；支持单选、多选、限制选、默认选择、管理、统计、汇总、测试、验证、打分 等等；

**相关文档：**  
- 《[option-model的教程][]》
- 《[option-model的接口文档][]》


**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe


# 二、安装方式
目前，安装方式有以下几种：


## 方式1：通过 npm 安装
```
npm install --save option-model
```

## 方式2：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，此仓库里包含了 option-model 和 下文的示例代码； option-model 库是 [本项目的Git仓库][] 项目中的 package/ChoiceQuestionModel.js 文件，您可以直接把该文件拷贝到您的项目中去；然后使用如下代码在您的项目中引入你需要的类：
```
import {ChoiceQuestion,Option,ChoiceQuestionList,ChoiceCondition} from "path/to/package/ChoiceQuestionModel.js";
```

